import cplex
from cplex.exceptions import CplexError
from docplex.mp.model import Model

#the follow is what I want to write to solve the general problem, but too difficult, so now I just focus on exact one example
#use n as hospital number,
# n = input('the maxium number of hospital could be: ')
# patient_num = input('the patient number of this instance is:')
# proc_1 = input('the processing of the 1st dose is: ')
# proc_2 = input('the processing of the 2nd dose is: ')
# gap = input('the gap is: ')
#
#
# class patient():
#
#     def __init__(self, patient_number, fir_start_time, fir_end_time, dely, sec_leg):
#         self.patient_number = patient_number
#         self.fir_start_time = fir_start_time
#         self.fir_end_time = fir_end_time
#         self.dely = dely
#         self.sec_leg = sec_leg
#     def printinfo(self):
#         print(self.fir_start_time,self.fir_end_time,self.dely,self.sec_leg)
#
# for i in patient:
#     fir_statr_time = input("the start of the first dose is: ")
#     fir_end_time = input("the end of the first dose is: ")
#     dely = input("the dely of patient is: ")
#     sec_leg = input("the length of the second dose is: ")
# patients = [{'interval0_length': 5, 'interval1_length': 2}]



#get instance example by bin problem:
#in this example, it get m=5 bin, the maximun of every bin is W=5000, and there is n=8 people want to transport the package, every customer's package's weight is weight = [1050, 1730, 2575, 3540, 1220, 1340, 1530, 1270], and we want to use mininum bin to complete this mission.
#it also use yi to say whether or not to have a new bin, and xij for i package used for j customer
#here is the code to solve this problem:

# from docplex.mp.model import Model

# m = 5
# n = 8
# M = [i for i in range(1,m+1)]
# N = [i for i in range(1,n+1)]
# A = {(i,j) for i in M for j in N}
# W = 5000
# weight = [1050, 1730, 2575, 3540, 1220, 1340, 1530, 1270]
# weight_dict = {i:weight[i-1] for i in N}

# # use model
# mdl = Model('MIP') #mdl是英文单词“model" 的缩写

# # variety
# y = mdl.binary_var_dict(M, name='y')
# x = mdl.binary_var_dict(A, name='x')

# #to minimize the result
# mdl.minimize(mdl.sum(y[i] for i in M))

# #constraints:
# mdl.add_constraints(mdl.sum(x[i,j] for i in M)==1 for j in N)
# mdl.add_constraints(mdl.sum(x[i,j]*weight_dict[j] for j in N) <= W*y[i] for i in M)

# #use model to solve the solution
# solution = mdl.solve()
# print(solution)

###################################here's the result:#######################
#objective: 3
#bin：y_2=1，y_3=1，y_4=1
#distribution：x_2_8=1，x_3_3=1， x_4_4=1， x_2_2=1， x_3_5=1， x_2_7=1， x_4_6=1， x_3_1=1


n = 4 #maxium hospital number
N = [i for i in range(1,n+1)]
p = 4 #patients number
P = [j for j in range(1,p+1)]
A = {(i,j)for i in N for j in P}

r11, d11, x1, l1 = 1, 4, 1, 4
r21, d21, x2, l2 = 2, 5, 3, 4
r31, d31, x3, l3 = 4, 8, 2, 3
r41, d41, x4, l4 = 1, 5, 4, 5
p1 = 3
p2 = 3
gap = 3

t11 = [i for i in range(r11, d11)]
t21 = [i for i in range(r21, d21)]
t31 = [i for i in range(r31, d31)]
t41 = [i for i in range(r41, d41)]

mdl = Model()
#use yi to consider whether to open a hospital_i or not
#use xij to consider which empty hospital i to serve patient j
y = mdl.binary_var_dict(N, name ='y')
x = mdl.binary_var_dict(A, name='x')

#purpose
mdl.minimize(mdl.sum(y[i] for i in N))

#restrictive
mdl.add_constraints(mdl.sum(x[i,j] for i in N)==1 for j in P)#one patient, one hospital
mdl.add_constraints(r11<=t11+p1-1<=d11)
mdl.add_constraints((d11-r11)>=p1)
#mdl.add_constraints(P1>=1)

#get model and show the result
solution = mdl.solve()
print(solution)






