# Vaccination Scheduling

[`src/`](src) contains a solution for *The online problem* (section 6.1.2) of [the
vaccine scheduling university project](https://gitlab.com/rdnz/algorithms_online/-/raw/main/specification.pdf).

## Run

Execute `cargo run submission_offline/hou_instance.txt`.
