with import
  (fetchTarball {
    name = "nixos-23.05-2023-08-10";
    url = "https://github.com/NixOS/nixpkgs/archive/18784aac1013da9b442adf29b6c7c228518b5d3f.tar.gz";
    sha256 = "0wjpj5v4i3ry0mkwai2fr3jqayhr7q45wrh2qjmca0ndamiikpzn";
  })
    {};
mkShell {
  packages = [rustc cargo rustfmt];
}
