use std::collections::BTreeSet;
use std::fmt;
use std::iter::{empty, once};
use std::ops::Range;

type Time = i32;
type Timetable = BTreeSet<(HospitalId, Time)>;

#[derive(Debug, PartialOrd, PartialEq, Eq, Ord, Clone, Copy)]
struct HospitalId(i32);

impl fmt::Display for HospitalId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug)]
struct State {
    processing0: Time,
    processing1: Time,
    gap: Time,
    timetable: Timetable,
}

#[derive(Debug)]
struct Patient {
    interval0_start: Time,
    interval0_length: Time,
    delay: Time,
    interval1_length: Time,
}

pub fn main(intput: &str) -> String {
    let (mut state, patients) = parse_input(&intput);
    println!("{:#?}", state);
    println!("{:#?}", patients);
    patients
        .iter()
        .map(|patient| process_patient(patient, &mut state))
        .map(|(time0, hospital_id0, time1, hospital_id1)| {
            format!("{}, {}, {}, {}\n", time0, hospital_id0, time1, hospital_id1)
        })
        .fold(String::from(""), |a, b| a + &b)
        + &format!("{}", count_hospitals(&(state.timetable)))
}

fn parse_input(text: &str) -> (State, Vec<Patient>) {
    let mut lines = text.lines();
    let processing0 = lines
        .next()
        .expect("first processing time needed")
        .parse()
        .expect("natural needed");
    let processing1 = lines
        .next()
        .expect("second processing time needed")
        .parse()
        .expect("natural needed");
    let gap = lines
        .next()
        .expect("gap needed")
        .parse()
        .expect("natural needed");
    (
        State {
            processing0,
            processing1,
            gap,
            timetable: BTreeSet::new(),
        },
        lines.filter_map(parse_patient).collect(),
    )
}

fn parse_patient(text: &str) -> Option<Patient> {
    let mut cells = text.split(",").map(str::trim).map(str::parse);
    let interval0_start = cells.next()?.ok()?;
    let interval0_length = cells.next()?.ok()? - interval0_start + 1;
    Some(Patient {
        interval0_start,
        interval0_length,
        delay: cells.next()?.ok()?,
        interval1_length: cells.next()?.ok()?,
    })
}

fn process_patient(patient: &Patient, state: &mut State) -> (Time, HospitalId, Time, HospitalId) {
    let state_immutable: &State = state;
    let possibilities = (patient.interval0_start
        ..(patient.interval0_start + patient.interval0_length - state.processing0 + 1))
        .flat_map(|time0| {
            let interval1_start = time0 + state.processing0 + state.gap + patient.delay;
            (interval1_start..(interval1_start + patient.interval1_length - state.processing1 + 1))
                .map(move |time1| {
                    let (hospital_id0, hospital_id1, timetable_new) =
                        add_to_timetable(time0, time1, state_immutable);
                    (time0, time1, hospital_id0, hospital_id1, timetable_new)
                })
        });
    let (_, (time0, time1, hospital_id0, hospital_id1, timetable)) = possibilities
        .map(|possibility| (count_hospitals(&possibility.4), possibility))
        // .rev()
        .min_by_key(|possibility| possibility.0)
        .expect("an appointment will always be found");
    state.timetable = timetable;
    (time0, hospital_id0, time1, hospital_id1)
}

fn add_to_timetable(
    dose0: Time,
    dose1: Time,
    state: &State,
) -> (HospitalId, HospitalId, Timetable) {
    fn add(time: Time, processing: Time, timetable: &mut Timetable) -> HospitalId {
        for hospital in 0.. {
            let hospital = HospitalId(hospital);
            if iterator_is_emtpy(timetable.range(Range {
                start: (hospital, time),
                end: (hospital, time + processing),
            })) {
                for time_current in time..(time + processing) {
                    timetable.insert((hospital, time_current));
                }
                return hospital;
            }
        }
        panic!("a hospital will always be found")
    }
    let mut result = state.timetable.clone();
    let hospital_id0 = add(dose0, state.processing0, &mut result);
    let hospital_id1 = add(dose1, state.processing1, &mut result);
    (hospital_id0, hospital_id1, result)
}

fn iterator_is_emtpy(mut iterator: impl Iterator) -> bool {
    iterator.next().is_none()
}

fn count_hospitals(timetable: &Timetable) -> usize {
    filter_adjacent_duplicates(timetable.iter().map(|(h, _)| h)).count()
}

fn filter_adjacent_duplicates<'a, T>(mut iterator: T) -> Box<dyn Iterator<Item = T::Item> + 'a>
where
    T: Iterator + Clone + 'a,
    T::Item: PartialEq,
{
    match iterator.next() {
        None => Box::new(empty()),
        Some(item0) => Box::new(
            once(item0).chain(
                iterator
                    .clone()
                    .skip(1)
                    .zip(iterator)
                    .filter(unequal_components)
                    .map(component0),
            ),
        ),
    }
}

fn unequal_components<T: PartialEq>(a: &(T, T)) -> bool {
    a.0 != a.1
}
fn component0<T, U>((t, _): (T, U)) -> T {
    t
}
