use std::env;
use std::fs;

fn main() {
    let file = env::args().nth(1).expect("command line argument needed");
    let content = fs::read_to_string(file).expect("readable file needed");
    println!("{}", algorithms_online::main(&content));
}
